# Generated by Django 3.1 on 2020-08-25 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='comments',
            field=models.CharField(default='comment', max_length=255),
        ),
    ]
